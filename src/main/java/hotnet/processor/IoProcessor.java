package hotnet.processor;

import hotnet.future.WriteFuture;
import hotnet.session.IoSession;

public interface IoProcessor {
	 boolean isDisposing();
	 boolean isDisposed();
	 
	 /* the processor managed by processor manager */
	 void dispose();
	 
	 void add(IoSession session);
	 void remove(IoSession session);
	 
	 void flush(IoSession session);
	 void write(IoSession session, WriteFuture writeRequest);
	 
}
