package hotnet.service;

import hotnet.session.IoSession;

import java.io.IOException;
import java.net.SocketAddress;
import java.util.List;
import java.util.Set;


public interface IoAcceptor extends IoService {
	Set<SocketAddress> getLocalAddresses();
	List<SocketAddress> getDefaultLocalAddresses();
	void setDefaultLocalAddresses(List<SocketAddress> localAddresses);
	
	/* bind default local address */
	void bind() throws IOException;
	void bind(int port) throws IOException;
	void bind(SocketAddress localAddress) throws IOException;
	void bind(List<SocketAddress> localAddresses) throws IOException;
	
	void unbind(SocketAddress localAddress);
	void unbind(List<SocketAddress> localAddresses);
	
	IoSession newSession(SocketAddress remoteAddress, SocketAddress localAddress);

}
