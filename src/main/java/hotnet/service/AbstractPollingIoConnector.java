package hotnet.service;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.channels.ClosedSelectorException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicReference;

import hotnet.config.IoSessionConfig;
import hotnet.config.SocketSessionConfig;
import hotnet.exception.ConnectionException;
import hotnet.exception.ExceptionMonitor;
import hotnet.future.ConnectFuture;
import hotnet.future.DefaultConnectFuture;
import hotnet.processor.IoProcessor;
import hotnet.processor.SimpleIoProcessorPool;
import hotnet.session.IoSession;

public abstract class AbstractPollingIoConnector extends AbstractIoService implements SocketConnector {
	private long connectTimeoutCheckInterval = 50L;
	private long connectTimeoutInMillis = 60 * 1000L; // 1 minute by default
	private SocketAddress defaultRemoteAddress;
	private SocketAddress defaultLocalAddress;
	
	private final Queue<ConnectionRequest> connectQueue = new ConcurrentLinkedQueue<ConnectionRequest>();
	private final Queue<ConnectionRequest> cancelQueue = new ConcurrentLinkedQueue<ConnectionRequest>();
	
	private final IoProcessor processor;
	
	private volatile boolean selectable = false;
	private final AtomicReference<Connector> connectorRef = new AtomicReference<Connector>();
	
	public AbstractPollingIoConnector(IoSessionConfig config, Class<? extends IoProcessor> processorCls) {
		super(config);
		this.processor = new SimpleIoProcessorPool(processorCls);
		
		try {
			init();
			selectable = true;
		} catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException("Failed to initialize.", e);
        } finally {
            if (!selectable) {
                try {
                    destroy();
                } catch (Exception e) {
                    ExceptionMonitor.getInstance().exceptionCaught(e);
                }
            }
        }
	}

	@Override
	public SocketSessionConfig getSessionConfig() {
		return (SocketSessionConfig) getDefaultSessionConfig();
	}

	@Override
	public void setDefaultRemoteAddress(InetSocketAddress remoteAddress) {
		this.defaultRemoteAddress = remoteAddress;
	}
	
	@Override
	public void setDefaultLocalAddress(InetSocketAddress localAddress) {
		this.defaultLocalAddress = localAddress;
	}

	@Override
	public long getConnectTimeoutMillis() {
		return connectTimeoutInMillis;
	}

	@Override
	public void setConnectTimeoutMillis(long connectTimeoutInMillis) {
		this.connectTimeoutInMillis = connectTimeoutInMillis;
	}
	
	public long getConnectTimeoutCheckInterval() {
		return connectTimeoutCheckInterval;
	}

	public void setConnectTimeoutCheckInterval(long connectTimeoutCheckInterval) {
		this.connectTimeoutCheckInterval = connectTimeoutCheckInterval;
	}

	public long getConnectTimeoutInMillis() {
		return connectTimeoutInMillis;
	}

	public void setConnectTimeoutInMillis(long connectTimeoutInMillis) {
		this.connectTimeoutInMillis = connectTimeoutInMillis;
	}

	public SocketAddress getDefaultRemoteAddress() {
		return defaultRemoteAddress;
	}

	public void setDefaultRemoteAddress(SocketAddress defaultRemoteAddress) {
		if (defaultRemoteAddress == null) {
			throw new IllegalArgumentException("remote address is null");
		}
		
		this.defaultRemoteAddress = defaultRemoteAddress;
	}
	
	public final ConnectFuture connect() {
		if (defaultRemoteAddress == null) {
			throw new IllegalStateException("default remote address not set");
		}
		
		return connect(defaultLocalAddress);
	}
	public final ConnectFuture connect(SocketAddress remoteAddress) {
		return connect(remoteAddress, null);
	}
	
	public final ConnectFuture connect(SocketAddress remoteAddress, SocketAddress localAddress) {
		if(isDisposed() || isDisposed())
			throw new IllegalStateException("The connector has been disposed.");
		
		if (remoteAddress == null) {
            throw new IllegalArgumentException("remoteAddress");
        }
		
		if (getHandler() == null) {
			throw new IllegalArgumentException("handler empty");
		}
		
		return connect0(remoteAddress, localAddress);
	}
	
	protected ConnectFuture connect0(SocketAddress remoteAddress, SocketAddress localAddress) {
		SocketChannel channel = null;
		boolean success = false;
		
		try {
			channel = newChannel(localAddress);
			if (connect(channel, remoteAddress)) {
				// 成功返回
				ConnectFuture future = new DefaultConnectFuture();
				
				IoSession session = newSession(processor, channel);
//				future.completeConnect(session);
				session.getProcessor().add(session);
				success = true;
				
                return future;
			}
			
			// 因为异步处理暂时没有响应
			success = true;
		} catch (Exception e) {
            return DefaultConnectFuture.newFailedFuture(e);
        } finally {
            if (!success && channel != null) {
                try {
                    close(channel);
                } catch (Exception e) {
                    ExceptionMonitor.getInstance().exceptionCaught(e);
                }
            }
        }
		
		ConnectionRequest future = new ConnectionRequest(channel);
		connectQueue.add(future);
		
		startupWorker();
		wakeup();
		
		return future;
	}
	
	private void startupWorker() {
		if (!selectable) {
			cancelQueue.clear();
			connectQueue.clear();
		}
		
		Connector connector = connectorRef.get();
		
		if (connector == null) {
			connector = new Connector();
			
			if (connectorRef.compareAndSet(null, connector)) {
				startRunService(connector);
			}
		}
	}
	
    protected final void dispose0() throws Exception {
        startupWorker();
        wakeup();
    }
	
	protected abstract void init() throws Exception;
	protected abstract void destroy() throws Exception;
	protected abstract SocketChannel newChannel(SocketAddress localAddress) throws Exception;
	protected abstract boolean connect(SocketChannel handle, SocketAddress remoteAddress) throws Exception;
	protected abstract void close(SocketChannel channel) throws Exception;
	protected abstract boolean finishConnect(SocketChannel handle) throws Exception;
	protected abstract IoSession newSession(IoProcessor processor, SocketChannel handle) throws Exception;
	protected abstract void wakeup();
	protected abstract int select(int timeout) throws Exception;
	protected abstract Iterator<SocketChannel> selectedHandles();
	protected abstract Iterator<SocketChannel> allHandles();
	protected abstract void register(SocketChannel channel, ConnectionRequest request) throws Exception;
	protected abstract ConnectionRequest getConnectionRequest(SocketChannel handle);
	
	public class ConnectionRequest extends DefaultConnectFuture {
		private static final int DEFAULT_CONNECT_TIMEOUT = 16000; // 6s
		private final SocketChannel channel;
		private final long deadline; 
		
		public ConnectionRequest(SocketChannel channel) {
			this.channel = channel;
			this.deadline = System.currentTimeMillis() + DEFAULT_CONNECT_TIMEOUT;
		}

		public SocketChannel getChannel() {
			return channel;
		}

		public long getDeadline() {
			return deadline;
		}
		
	}
	
	private int registerNew() {
		int nHandle = 0;
		
		for (;;) {
			ConnectionRequest request = connectQueue.poll();
			
			if (request == null) 
				break;
			
			SocketChannel channel = request.getChannel();
			
			try {
				register(channel, request);
				
				nHandle++;
			} catch(Exception e) {
				request.setException(e);
				
				try {
					close(channel);
				} catch (Exception e2) {
					ExceptionMonitor.getInstance().exceptionCaught(e2);
				}
			}
		}
		
		return nHandle;
	}
	
	private int processConnections(Iterator<SocketChannel> selectedChannel) {
		SocketChannel channel = null;
		int nHandles = 0;
		
		while (selectedChannel.hasNext()) {
			channel = selectedChannel.next();
			selectedChannel.remove();
			ConnectionRequest request = getConnectionRequest(channel);
			
			if (request == null) {
				continue;
			}
			
			boolean success = false;
			
			try {
				if (finishConnect(channel)) {
					IoSession session = newSession(processor, channel);
					initSession(session, request);
//					request.completeConnect(session);
					session.getProcessor().add(session);
					nHandles++;
				}
				
				success = true;
			} catch(Exception e) {
				request.setException(e);
			} finally {
				if (!success) {
					cancelQueue.offer(request);
				}
			}
		}
		
		return nHandles;
	}
	
	private void processTimedOutSessions(Iterator<SocketChannel> allChannel) {
		long currentTime = System.currentTimeMillis();
		
		while (allChannel.hasNext()) {
			SocketChannel channel = allChannel.next();
			
			ConnectionRequest request = getConnectionRequest(channel);
			
			if (request == null) {
				continue;
			}
			
			if (currentTime >= request.getDeadline()) {
				Throwable aThrowable = new ConnectionException("connection time out");
				request.setException(aThrowable);
				cancelQueue.offer(request);
			}
		}
	}
	
	private int cancelKeys() {
		int nHandles = 0;
		
		while (!cancelQueue.isEmpty()) {
			ConnectionRequest request = cancelQueue.poll();
			if (request == null) {
				break;
			}
			
			SocketChannel channel = request.getChannel();
			try {
				close(channel);
			} catch (Exception e) {
                ExceptionMonitor.getInstance().exceptionCaught(e);
            } finally {
                nHandles++;
            }
		}
		
		return nHandles;
	}
	
	private class Connector implements Runnable {

		@Override
		public void run() {
			int nHandles = 0;

			while (selectable) {
				try {
					int timeout = (int) Math.min(getConnectTimeoutInMillis(),
							1000);
					int selected = select(timeout);
					nHandles += registerNew();

					if (nHandles == 0) {
						connectorRef.set(null);

						if (connectQueue.isEmpty()) {
							break;
						}

						if (!connectorRef.compareAndSet(null, this)) {
							break;
						}
					}

					if (selected > 0) {
						nHandles -= processConnections(selectedHandles());
					}

					processTimedOutSessions(allHandles());

					nHandles -= cancelKeys();
					
				} catch (ClosedSelectorException cse) {
					ExceptionMonitor.getInstance().exceptionCaught(cse);
				} catch (Exception e) {
					ExceptionMonitor.getInstance().exceptionCaught(e);

					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						ExceptionMonitor.getInstance().exceptionCaught(e1);
					}
				}
			}

			if (selectable && isDisposing()) {
				selectable = false;

				processor.dispose();

				try {
					synchronized (disposalLock) {
						if (isDisposing()) {
							destroy();
						}
					}
				} catch (Exception e) {
					ExceptionMonitor.getInstance().exceptionCaught(e);
				}
			}
		}

	}

}
