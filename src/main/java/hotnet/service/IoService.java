package hotnet.service;

import java.util.Map;

import hotnet.common.IoServiceStatistics;
import hotnet.config.IoSessionConfig;
import hotnet.filter.IoFilterChain;
import hotnet.filter.IoFilterChainBuilder;
import hotnet.handler.IoHandler;
import hotnet.listener.IoServiceListener;
import hotnet.listener.IoServiceListenerManager;
import hotnet.session.IoSession;

public interface IoService {
	void addListener(IoServiceListener listener);
	void removeListener(IoServiceListener listener);
	IoServiceListenerManager getListenerManager();
	
	/* service at less has one session */
	boolean isActive();
	boolean isDisposing();
	boolean isDisposed();
	
	void dispose();
	void dispose(boolean awaitTermination);
	
	IoHandler getHandler();
	void setHandler(IoHandler handler);
	IoService getService();
	
	Map<Long, IoSession> getManagedSessions();
	IoSessionConfig getDefaultSessionConfig();
	
	IoFilterChainBuilder getIoFilterChainBuilder();
	
	IoServiceStatistics getStatistics();
}
