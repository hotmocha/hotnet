package hotnet.service;

import hotnet.config.SocketSessionConfig;

import java.net.InetSocketAddress;

public interface SocketConnector extends IoConnector {
	SocketSessionConfig getSessionConfig();
	void setDefaultRemoteAddress(InetSocketAddress remoteAddress);
	void setDefaultLocalAddress(InetSocketAddress localAddress);
}
