package hotnet.service;

import hotnet.config.SocketSessionConfig;

public interface SocketAcceptor extends IoAcceptor {
	boolean isReuseAddress();
	void setReuseAddress(boolean reuseAddress);
	int getBacklog();
	void setBacklog(int backlog);
	SocketSessionConfig getSocketSessionConfig();
}
