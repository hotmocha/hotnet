package hotnet.service;

import hotnet.future.ConnectFuture;

import java.net.SocketAddress;

public interface IoConnector extends IoService {
	long getConnectTimeoutMillis();
	void setConnectTimeoutMillis(long connectTimeoutInMillis);
	
	ConnectFuture connect();
	ConnectFuture connect(SocketAddress remoteAddress);
	ConnectFuture connect(SocketAddress remoteAddress, SocketAddress localAddress);
}
