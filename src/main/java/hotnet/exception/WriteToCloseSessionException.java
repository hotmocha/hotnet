package hotnet.exception;

import hotnet.future.WriteFuture;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WriteToCloseSessionException extends IOException {
	private static final long serialVersionUID = 1L;
	private final List<WriteFuture> requests;
	
	public WriteToCloseSessionException(List<WriteFuture> requests, String message, Throwable cause) {
		super(message);
		initCause(cause);
		
		this.requests = asRequestList(requests);
	}
	
	public WriteToCloseSessionException(WriteFuture requests, String message, Throwable cause) {
		super(message);
		initCause(cause);
		
		this.requests = asRequestList(requests);
	}
	
	public WriteToCloseSessionException(List<WriteFuture> requests, String message) {
		super(message);
		
		this.requests = asRequestList(requests);
	}
	
	public WriteToCloseSessionException(List<WriteFuture> requests) {
		super();
		
		this.requests = asRequestList(requests);
	}
	
    public List<WriteFuture> getRequests() {
        return requests;
    }
	
	private static List<WriteFuture> asRequestList(List<WriteFuture> requests) {
        if (requests == null) {
            throw new IllegalArgumentException("requests");
        }

        return Collections.unmodifiableList(requests);
    }
	
	private static List<WriteFuture> asRequestList(WriteFuture request) {
        if (request == null) {
            throw new IllegalArgumentException("request");
        }

        List<WriteFuture> requests = new ArrayList<WriteFuture>(1);
        requests.add(request);
        return Collections.unmodifiableList(requests);
    }
}
