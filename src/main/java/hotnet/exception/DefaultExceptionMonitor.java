package hotnet.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultExceptionMonitor extends ExceptionMonitor {
	private final static Logger LOGGER = LoggerFactory.getLogger(DefaultExceptionMonitor.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void exceptionCaught(Throwable cause) {
        if (cause instanceof Error) {
            throw (Error) cause;
        }

        LOGGER.warn("Unexpected exception.", cause);
    }
}
