package hotnet.exception;

public class CloseChannelException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7520718394751031104L;

	public CloseChannelException(String msg) {
		super(msg);
	}
}
