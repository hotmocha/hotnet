package hotnet.exception;

public class ConnectionException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5100297011805216830L;
	
	public ConnectionException(String msg) {
		super(msg);
	}
}
