package hotnet.common;

public class NamedRunnable implements Runnable {
	private String name;
	private Runnable runnable;
	
	public NamedRunnable(Runnable runnable, String name) {
		this.name = name;
		this.runnable = runnable;
	}
	
	@Override
	public void run() {
		String oldName = Thread.currentThread().getName();
		
		Thread.currentThread().setName(name);
		try {
			this.runnable.run();
		} finally {
			Thread.currentThread().setName(oldName);
		}
	}

}
