package hotnet.common;

public enum SessionState {
    OPENING, OPENED, CLOSING
}