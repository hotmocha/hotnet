package hotnet.config;

public interface SocketSessionConfig extends IoSessionConfig {
	boolean isReuseAddress();
	void setReuseAddress(boolean reuseAddress);
	boolean isBlocking();
	void setBlocking(boolean blocking);
	int getReceiveBufferSize();
	void setReceiveBufferSize(int receiveBufferSize);
	int getSendBufferSize();
	void setSendBufferSize(int sendBufferSize);
	boolean isKeepAlive();
	void setKeepAlive(boolean keepAlive);
	int getSoLinger();
	void setSoLinger(int soLinger);
	boolean isTcpNoDelay();
	void setTcpNoDelay(boolean tcpNoDelay);
}
