package hotnet.config;

public abstract class AbstractIoSocketSessionConfig extends AbstractIoSessionConfig implements SocketSessionConfig {
	@Override
    protected final void doSetOtherAll(IoSessionConfig config) {
        if (!(config instanceof SocketSessionConfig)) {
            return;
        }

        if (config instanceof AbstractIoSocketSessionConfig) {
        	AbstractIoSocketSessionConfig cfg = (AbstractIoSocketSessionConfig) config;
            if (cfg.isKeepAliveChanged()) {
                setKeepAlive(cfg.isKeepAlive());
            }
            if (cfg.isReceiveBufferSizeChanged()) {
                setReceiveBufferSize(cfg.getReceiveBufferSize());
            }
            if (cfg.isReuseAddressChanged()) {
                setReuseAddress(cfg.isReuseAddress());
            }
            if (cfg.isSendBufferSizeChanged()) {
                setSendBufferSize(cfg.getSendBufferSize());
            }
            if (cfg.isSoLingerChanged()) {
                setSoLinger(cfg.getSoLinger());
            }
            if (cfg.isTcpNoDelayChanged()) {
                setTcpNoDelay(cfg.isTcpNoDelay());
            }
            if (cfg.isBlockingChanged()) {
            	setBlocking(cfg.isBlocking());
            }
            
        } else {
            SocketSessionConfig cfg = (SocketSessionConfig) config;
            setKeepAlive(cfg.isKeepAlive());
            setReceiveBufferSize(cfg.getReceiveBufferSize());
            setReuseAddress(cfg.isReuseAddress());
            setSendBufferSize(cfg.getSendBufferSize());
            setSoLinger(cfg.getSoLinger());
            setTcpNoDelay(cfg.isTcpNoDelay());
        }
    }
	
	protected boolean isKeepAliveChanged() {
		return true;
	}
	
	protected boolean isReceiveBufferSizeChanged() {
		return true;
	}
	
	protected boolean isReuseAddressChanged() {
		return true;
	}
	
	protected boolean isSendBufferSizeChanged() {
		return true;
	}

	protected boolean isSoLingerChanged() {
		return true;
	}

	protected boolean isTcpNoDelayChanged() {
		return true;
	}

	protected boolean isBlockingChanged() {
		return true;
	}
}
