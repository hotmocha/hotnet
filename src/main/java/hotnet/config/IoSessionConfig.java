package hotnet.config;

public interface IoSessionConfig {
	int getReadBufferSize();
	void setReadBufferSize(int readBufferSize);
	void setAll(IoSessionConfig config);
}
