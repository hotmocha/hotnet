package hotnet.config;


public abstract class AbstractIoSessionConfig implements IoSessionConfig {
	private int readBufferSize = 2048;
	 
	public final void setAll(IoSessionConfig config) {
		if (config == null) {
			throw new IllegalArgumentException("config cannot be null");
		}
		
		setReadBufferSize(readBufferSize);
		
		doSetOtherAll(config);
	}
	
	protected abstract void doSetOtherAll(IoSessionConfig config);
	
    public int getReadBufferSize() {
        return readBufferSize;
    }
    
    public void setReadBufferSize(int readBufferSize) {
        if (readBufferSize <= 0) {
            throw new IllegalArgumentException("readBufferSize: " + readBufferSize + " (expected: 1+)");
        }
        this.readBufferSize = readBufferSize;
    }
}
