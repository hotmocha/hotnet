package hotnet.config;

public class DefaultIoSocketSessionConfig extends AbstractIoSocketSessionConfig implements SocketSessionConfig {
    private static boolean DEFAULT_REUSE_ADDRESS = false;
    private static boolean DEFAULT_KEEP_ALIVE = false;
    private static int DEFAULT_SO_LINGER = -1;
    private static boolean DEFAULT_TCP_NO_DELAY = false;
    private static boolean DEFAULT_BLOCKING = true;
    
    private boolean reuseAddress = DEFAULT_REUSE_ADDRESS;
    private int receiveBufferSize = -1;
    private int sendBufferSize = -1;
    private boolean keepAlive = DEFAULT_KEEP_ALIVE;
    private int soLinger = DEFAULT_SO_LINGER;
    private boolean tcpNoDelay = DEFAULT_TCP_NO_DELAY;
    private boolean blocking = false;
    
	public boolean isReuseAddress() {
		return reuseAddress;
	}
	public void setReuseAddress(boolean reuseAddress) {
		this.reuseAddress = reuseAddress;
	}
	public int getReceiveBufferSize() {
		return receiveBufferSize;
	}
	public void setReceiveBufferSize(int receiveBufferSize) {
		this.receiveBufferSize = receiveBufferSize;
	}
	public int getSendBufferSize() {
		return sendBufferSize;
	}
	public void setSendBufferSize(int sendBufferSize) {
		this.sendBufferSize = sendBufferSize;
	}
	public boolean isKeepAlive() {
		return keepAlive;
	}
	public void setKeepAlive(boolean keepAlive) {
		this.keepAlive = keepAlive;
	}
	public int getSoLinger() {
		return soLinger;
	}
	public void setSoLinger(int soLinger) {
		this.soLinger = soLinger;
	}
	public boolean isTcpNoDelay() {
		return tcpNoDelay;
	}
	public void setTcpNoDelay(boolean tcpNoDelay) {
		this.tcpNoDelay = tcpNoDelay;
	}
	
	@Override
	public boolean isBlocking() {
		return blocking;
	}
	@Override
	public void setBlocking(boolean blocking) {
		this.blocking = blocking;
		
	}
	
	protected boolean isKeepAliveChanged() {
		return keepAlive != DEFAULT_KEEP_ALIVE;
	}
	
	protected boolean isReceiveBufferSizeChanged() {
		return receiveBufferSize != -1;
	}
	
	protected boolean isReuseAddressChanged() {
		return reuseAddress != DEFAULT_REUSE_ADDRESS;
	}
	
	protected boolean isSendBufferSizeChanged() {
		return sendBufferSize != -1;
	}

	protected boolean isSoLingerChanged() {
		return soLinger != DEFAULT_SO_LINGER;
	}

	protected boolean isTcpNoDelayChanged() {
		return tcpNoDelay != DEFAULT_TCP_NO_DELAY;
	}
	
	protected boolean isBlockingChanged() {
		return blocking != DEFAULT_BLOCKING;
	}
}
