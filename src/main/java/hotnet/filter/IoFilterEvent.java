package hotnet.filter;

import hotnet.filter.IoFilter.NextFilter;
import hotnet.future.WriteFuture;
import hotnet.session.IoSession;

public class IoFilterEvent extends IoEvent {
	private NextFilter nextFilter;

	public IoFilterEvent(NextFilter nextFilter, IoEventType eventType, IoSession session,
			Object parameter) {
		super(eventType, session, parameter);
		
		this.nextFilter = nextFilter;
	}
	
	public NextFilter getNextFilter() {
		return this.nextFilter;
	}
	
	@Override
	public void fire() {
		IoSession session = getSession();
		NextFilter nextFilter = getNextFilter();
		Object parameter = getParameter();
		
		switch(getType()) {
		case MESSAGE_RECEIVED:
			nextFilter.messageReceived(session, parameter);
			break;
		case MESSAGE_SENT:
			nextFilter.messageSent(session, (WriteFuture)parameter);
			break;	
		case WRITE:
			nextFilter.filterWrite(session, (WriteFuture)parameter);
            break;
        case CLOSE:
            nextFilter.filterClose(session);
            break;
        case EXCEPTION_CAUGHT:
        	nextFilter.exceptionCaught(session, (Throwable)parameter);
            break;
        case SESSION_OPENED:
        	nextFilter.sessionOpened(session);
            break;
        case SESSION_CREATED:
        	nextFilter.sessionCreated(session);
            break;
        case SESSION_CLOSED:
        	nextFilter.sessionClosed(session);
            break;
        default:
            throw new IllegalArgumentException("Unknown event type: " + getType());
		}
	}
	
}
