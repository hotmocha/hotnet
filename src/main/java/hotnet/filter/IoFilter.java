package hotnet.filter;

import hotnet.future.WriteFuture;
import hotnet.session.IoSession;

public interface IoFilter {
	String getName();
	void setName(String name);
	
	void init() throws Exception;
	void destory() throws Exception;
	
	void sessionCreated(NextFilter nextFilter, IoSession session) throws Exception;
	void sessionOpened(NextFilter nextFilter, IoSession session) throws Exception;
	void sessionClosed(NextFilter nextFilter, IoSession session) throws Exception;
	// void sessionIdle(NextFilter nextFilter, IoSession session, IdleStatus status);
	void messageReceived(NextFilter nextFilter, IoSession session, Object message) throws Exception;
	void messageSent(NextFilter nextFilter, IoSession session, WriteFuture future) throws Exception;
	void inputClosed(NextFilter nextFilter, IoSession session) throws Exception;
	void filterClose(NextFilter nextFilter, IoSession session) throws Exception;
	void filterWrite(NextFilter nextFilter, IoSession session, WriteFuture future) throws Exception;
	void exceptionCaught(NextFilter nextFilter, IoSession session, Throwable cause) throws Exception;

	interface NextFilter {
		void sessionCreated(IoSession session);
		void sessionOpened(IoSession session);
		void sessionClosed(IoSession session);
		// void sessionIdle(IoSession session, IdleStatus status);
		void messageReceived(IoSession session, Object message);
		void messageSent(IoSession session, WriteFuture future);
		void inputClosed(IoSession session);
		void filterClose(IoSession session);
		void filterWrite(IoSession session, WriteFuture future);
		void exceptionCaught(IoSession session, Throwable cause);
	}
}
