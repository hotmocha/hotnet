package hotnet.filter;

import hotnet.future.WriteFuture;
import hotnet.session.IoSession;

public class IoFilterAdapter implements IoFilter {

	private String name;
	
	public IoFilterAdapter(String name) {
	    this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public void init() throws Exception{
		
	}
	
	@Override
	public void destory() throws Exception {
		
	}

	@Override
	public void sessionCreated(NextFilter nextFilter, IoSession session) throws Exception{
		nextFilter.sessionCreated(session);
	}

	@Override
	public void sessionOpened(NextFilter nextFilter, IoSession session) throws Exception{
		nextFilter.sessionOpened(session);
	}

	@Override
	public void sessionClosed(NextFilter nextFilter, IoSession session) throws Exception{
		nextFilter.sessionClosed(session);
	}

	@Override
	public void messageReceived(NextFilter nextFilter, IoSession session, Object message) throws Exception {
		nextFilter.messageReceived(session, message);
	}

	@Override
	public void messageSent(NextFilter nextFilter, IoSession session, WriteFuture future) throws Exception{
		nextFilter.messageSent(session, future);
	}

	@Override
	public void exceptionCaught(NextFilter nextFilter, IoSession session, Throwable cause) throws Exception{
		nextFilter.exceptionCaught(session, cause);
	}

	@Override
	public void filterClose(NextFilter nextFilter, IoSession session) throws Exception {
		nextFilter.filterClose(session);
	}

	@Override
	public void filterWrite(NextFilter nextFilter, IoSession session, WriteFuture future) throws Exception{
		nextFilter.filterWrite(session, future);
		
	}

	@Override
	public void inputClosed(NextFilter nextFilter, IoSession session) throws Exception {
		nextFilter.inputClosed(session);
		
	}

}
