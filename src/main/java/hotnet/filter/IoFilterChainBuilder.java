package hotnet.filter;

/**
 * Created by hotmocha on 17/4/12.
 */
public interface IoFilterChainBuilder {
    void buildFilterChain(IoFilterChain chain);
    IoFilter getFilter(String name);
    void addFirst(IoFilter filter);
    void addLast(IoFilter filter);
    void addBefore(String baseName, IoFilter filter);
    void addAfter(String baseName, IoFilter filter);
    IoFilter replace(String name, IoFilter newFilter);
    IoFilter remove(String name);
    void clear() throws Exception;
}
