package hotnet.filter;

import hotnet.session.IoSession;

public abstract class IoEvent implements Runnable {
	private IoEventType eventType;
	private IoSession session;
	private Object parameter;

	public IoEvent(IoEventType eventType, IoSession session, Object parameter) {
        if (eventType == null) {
            throw new IllegalArgumentException("type");
        }
        if (session == null) {
            throw new IllegalArgumentException("session");
        }
        
		this.eventType = eventType;
		this.session = session;
		this.parameter = parameter;
	}
	
	public IoSession getSession() {
		return session;
	}
	
	public IoEventType getType() {
		return eventType;
	}
	
	public Object getParameter() {
		return parameter;
	}
	
	@Override
	public void run() {
		fire();
	}
	
	public abstract void fire();
}
