package hotnet.filter;

import java.util.List;

import hotnet.future.WriteFuture;
import hotnet.session.IoSession;

public interface IoFilterChain {
	IoSession getSession();

	void addFirst(IoFilter filter);
	void addLast(IoFilter filter);
	void addBefore(String baseName, IoFilter filter);
	void addAfter(String baseName, IoFilter filter);
	IoFilter remove(String name);
	void clear() throws Exception;
	List<Entry> getAll();
	List<Entry> getAllReverse();
	
	void fireSessionCreated();
	void fireSessionOpened();
	void fireSessionClosed();
	void fireMessageReceived(Object message);
	void fireMessageSent(WriteFuture request);
	void fireExceptionCaught(Throwable cause);
	
	void fireFilterClose();
	void fireInputClosed();
	void fireFilterWrite(WriteFuture writeRequest);

	public interface Entry {
		IoFilter getFilter();

		IoFilter.NextFilter getNextFilter();

		void addAfter(IoFilter filter);
		void addBefore(IoFilter filter);
		void remove();
	}
}
