package hotnet.listener;

import java.io.IOException;

import hotnet.service.IoService;
import hotnet.session.IoSession;

public interface IoServiceListener {
	void serviceActivated(IoService service) throws Exception;
	void serviceDeactivated(IoService service) throws Exception;
	
	void sessionCreate(IoSession session) throws Exception;
	void sessionClosed(IoSession session) throws Exception;
	void sessionDestroyed(IoSession session) throws Exception;
}
