package hotnet.listener;

import hotnet.future.IoFuture;

import java.util.EventListener;

public interface FutureListener extends EventListener {
	void operationComplete(IoFuture future) throws Exception;
}
