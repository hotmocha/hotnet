package hotnet.handler;

import hotnet.session.IoSession;

public interface IoHandler {
	void exceptionCaught(IoSession session, Throwable cause) throws Exception;
	
    void sessionCreated(IoSession session) throws Exception;
    void sessionOpened(IoSession session) throws Exception;
    void sessionClosed(IoSession session) throws Exception;
    
    void messageReceived(IoSession session, Object message) throws Exception;
    void messageSent(IoSession session, Object message) throws Exception;
    void inputClosed(IoSession session) throws Exception;
}
