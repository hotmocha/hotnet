package hotnet.handler;

import hotnet.session.IoSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IoHandlerAdapter implements IoHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(IoHandlerAdapter.class);

    public void sessionCreated(IoSession session) throws Exception {
        // Empty handler
    }

    public void sessionOpened(IoSession session) throws Exception {
        // Empty handler
    }

    public void sessionClosed(IoSession session) throws Exception {
        // Empty handler
    }

    public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
        if (LOGGER.isWarnEnabled()) {
            LOGGER.warn("EXCEPTION, please implement " + getClass().getName()
                    + ".exceptionCaught() for proper handling:", cause);
        }
    }

    public void messageReceived(IoSession session, Object message) throws Exception {
        // Empty handler
    }

    public void messageSent(IoSession session, Object message) throws Exception {
        // Empty handler
    }

    public void inputClosed(IoSession session) throws Exception {
        session.close(true);
    }
}