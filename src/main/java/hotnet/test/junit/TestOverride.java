package hotnet.test.junit;
class A {
	public void fun() {
		System.out.println("a");
	}
}
class B extends A {
	@Override
	public void fun() {
		System.out.println("b");
	}
}
public class TestOverride {
	public static void main(String[] args) {
		A a = new A();
		a.fun();
		
		B b = new B ();
		b.fun();
	}
}
