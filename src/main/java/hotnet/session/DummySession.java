package hotnet.session;

import hotnet.processor.IoProcessor;
import hotnet.service.IoService;

import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

public class DummySession extends AbstractIoSession {
	
	public DummySession() {
		this(null, null);
	}
	public DummySession(IoProcessor processor, IoService service) {
		super(processor, service);
	}

	@Override
	public SocketChannel getChannel() {
		return null;
	}

	@Override
	public SelectionKey getSelectionKey() {
		return null;
	}

	@Override
	public void setSelectionKey(SelectionKey key) {		
	}

}
