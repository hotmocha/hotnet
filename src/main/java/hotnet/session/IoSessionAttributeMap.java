package hotnet.session;

import java.util.Set;

public interface IoSessionAttributeMap {
	Object getAttribute(Object key, Object defaultValue);
	Object setAttribute(Object key, Object value);
	Object setAttributeIfAbsent(Object key, Object value);
	Object removeAttribute(Object key);
	boolean removeAttribute(Object key, Object value);
	boolean replaceAttribute(Object key, Object oldValue, Object newValue);
	boolean containsAttribute(Object key);
	Set<Object> getAttributeKeys();
}
