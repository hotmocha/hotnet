package hotnet.session;

import hotnet.service.IoService;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class IoSessionManager {
	private final IoService service;
	private final ConcurrentMap<Long, IoSession> managedSessions = new ConcurrentHashMap<Long, IoSession>();
		
	public IoSessionManager(IoService service) {
		this.service = service;
	}
	
	public void add(IoSession session) {
		if (session != null) {
			managedSessions.putIfAbsent(session.getSessionId(), session);
		}
	}
	
	public void remove(IoSession session) {
		if (session != null) {
			managedSessions.remove(session.getSessionId());
		}
	}
	
	public int getManagedSessionCount() {
		return managedSessions.size();
	}
	
	public Map<Long, IoSession> getManagedSessions() {
		return this.managedSessions;
	}

	public IoService getService() {
		return service;
	}
	
}
