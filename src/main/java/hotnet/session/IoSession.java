package hotnet.session;

import java.net.SocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Set;

import hotnet.config.IoSessionConfig;
import hotnet.filter.IoFilterChain;
import hotnet.future.CloseFuture;
import hotnet.future.WriteFuture;
import hotnet.handler.IoHandler;
import hotnet.processor.IoProcessor;
import hotnet.service.IoService;

public interface IoSession {
	long getSessionId();

	IoProcessor getProcessor();
	
	IoFilterChain getFilterChain();
	
	IoSessionConfig getSessionConfig();
	
	IoHandler getHandler();
	SocketChannel getChannel();
	IoService getService();
	
	SocketAddress getLocalAddress();
	SocketAddress getRemoteAddress();
	
	SelectionKey getSelectionKey();
	void setSelectionKey(SelectionKey key);
	
	WriteFuture write(Object msg);
	CloseFuture close();
	CloseFuture close(boolean rightNow);
	
	boolean isClosing();
	boolean isClosed();
	boolean isConnected();
	
	Object getAttribute(Object key, Object defaultValue);
	Object setAttribute(Object key, Object value);
	Object setAttributeIfAbsent(Object key, Object value);
	Object removeAttribute(Object key);
	boolean removeAttribute(Object key, Object value);
	boolean replaceAttribute(Object key, Object oldValue, Object newValue);
	boolean containsAttribute(Object key);
	Set<Object> getAttributeKeys();	
}
