package hotnet.buffer;

import java.nio.ByteBuffer;

public interface IoBufferAllocator {
	IoBuffer allocate(int capacity, boolean direct);
	ByteBuffer allocateNioBuffer(int capacity, boolean direct);
	IoBuffer wrap(ByteBuffer nioBuffer);
}
