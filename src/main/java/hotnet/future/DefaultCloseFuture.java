package hotnet.future;

import hotnet.session.IoSession;


public class DefaultCloseFuture extends DefaultIoFuture implements CloseFuture {
	
	public DefaultCloseFuture(IoSession session) {
		super(session);
	}
	
    public boolean isClosed() {
        if (isDone()) {
            return ((Boolean) getValue()).booleanValue();
        } else {
            return false;
        }
    }

    public void setClosed() {
        setValue(Boolean.TRUE);
    }

}
