package hotnet.future;

import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AcceptorFuture extends DefaultIoFuture {
	private final List<SocketAddress> localAddresses;
	
	public AcceptorFuture(List<SocketAddress> localAddresses) {
		super(null);
		this.localAddresses = new ArrayList<SocketAddress>(localAddresses);
	}
	
    public final List<SocketAddress> getLocalAddresses() {
        return Collections.unmodifiableList(localAddresses);
    }
	
}
