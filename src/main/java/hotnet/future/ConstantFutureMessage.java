package hotnet.future;

public class ConstantFutureMessage {
	public final static ConstantCloseObject closeObject = new ConstantCloseObject();
	
	public final static class ConstantCloseObject {
		private ConstantCloseObject() {
		}
	}
}
