package hotnet.future;

import hotnet.session.IoSession;

public interface ConnectFuture extends IoFuture {
	boolean isConnected();
	boolean isCanceled();
	void cancel();
	void completeConnect(IoSession session);
}
