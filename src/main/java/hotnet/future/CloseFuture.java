package hotnet.future;

public interface CloseFuture extends IoFuture {
	
	boolean isClosed();
	
	void setClosed();
}
