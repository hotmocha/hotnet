package hotnet.future;

import java.net.SocketAddress;

public interface WriteFuture extends IoFuture {

	boolean isWritten();
	
	void setWritten();
	
	Object getMessage();
	
	SocketAddress getDestination();

}
