package hotnet.future;

import java.net.SocketAddress;

import hotnet.session.IoSession;


public class DefaultWriteFuture extends DefaultIoFuture implements WriteFuture {
	private Object message;
	
	public DefaultWriteFuture(IoSession session, Object message) {
		super(session);
		
		this.message = message;
		setWritten();
	}

	@Override
    public boolean isWritten() {
        if (isDone()) {
            Object v = getValue();
            if (v instanceof Boolean) {
                return ((Boolean) v).booleanValue();
            }
        }
        
        return false;
    }
	
    public void setWritten() {
        setValue(Boolean.TRUE);
    }

	@Override
	public Object getMessage() {
		return message;
	}

	@Override
	public SocketAddress getDestination() {
		return getSession().getRemoteAddress();
	}
}
