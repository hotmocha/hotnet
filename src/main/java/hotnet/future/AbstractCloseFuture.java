package hotnet.future;

public abstract class AbstractCloseFuture implements IoFuture {
    public abstract boolean isConnected();
    public abstract boolean isCanceled();
}
